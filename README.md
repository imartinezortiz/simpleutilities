# Utilidades simples para PHP (con propósito educativo)

Este proyecto es una pequeña biblioteca creada con propósitos educativos para ser usada en los cursos de desarrollo
web que imparto en la [Facultad de Informática de la Universidad Complutense de Madrid](https://informatica.ucm.es).

El proyecto es suficientemente simple, de modo que **sea simple de revisar el código fuente** por parte de nuevos desarrolladores de PHP.

Para un proyecto realista, se recomienda revisar un marco de trabajo general, bien probado y utilizado en proyectos reales, deberías echar un vistazo (en ningún orden particular): [Symphony Form library](https://symfony.com/), [Laravel](https://laravel.com).

Descarga el proyecto y colócalo dentro del `DocumentRoot` de tu servidor y visita [http://localhost/SimpleUtilities/demo/](http://localhost/SimpleUtilities/demo/)

# Simple utilities for PHP (for educational purposes only)

This project is a simple library created for educational purposes and used in the web development courses I teach at [Facultad de Informática of Universidad Complutense de Madrid](https://informatica.ucm.es). [Symphony Form library](https://symfony.com/), [Laravel](https://laravel.com).

It is simple enough, so **it is easy to review the source code** even for new PHP programmers.

For a general, well tested and production ready framework have a look to (in no particular order): [Symphony Form library](https://symfony.com/), [Laravel](https://laravel.com).