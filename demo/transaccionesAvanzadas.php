<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw;

require_once __DIR__.'/includes/config.php';

$proveedorConexiones = Aplicacion::getSingleton();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Transacciones avanzadas</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js"></script>
    <!-- and it's easy to individually load additional languages -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/languages/php.min.js"></script>
    <script>
        hljs.highlightAll();
    </script>
</head>
<body>
    <h1>Transacciones Avanzadas</h1>
    <p>Como hemos comentado previamente, el uso de transacciones requiere copiar y pegar código a modo de plantilla
    para gestionar las transacciones. No obstante, haciendo uso de la capacidades de PHP de definir funciones anónimas
    y de poder invocarlas desde dentro del código, podemos simplificar bastante la gestión de las transacciones. Este tipo
    de funciones recibe el nombre de <a href="https://www.php.net/manual/en/language.types.callable.php">callable</a></p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
namespace es\\ucm\\fdi\\aw\\sql;

// \$proveedorConexiones implementa la interfaz MySQLConnectionProvider
\$transactionTemplate = new MySQLTransactionTemplate(\$proveedorConexiones);

try {
    \$transactionTemplate->execute(function(MySQLConnectionProvider \$provider) {
    // Obtenemos la conexión
    \$conn = \$provider->getConnection();

    // Ejecutamos las operaciones
    \$rs = \$conn->query('SELECT * FROM Productos WHERE id=1');
    if (\$rs) {
        \$filaProducto = \$rs->fetch_assoc();
        \$rs->free();
    }

    if (\$filaProducto['unidadesDisponibles'] > 0 ) {
        // Actualizamos la tabla producto
        \$conn->query('UPDATE Productos SET unidadesDisponibles=(unidadesDisponibles-1) WHERE id=1 AND unidadesDisponibles > 0') !== true;
        \$productoDisponible = \$conn->affected_rows > 0;
    }

    if (\$productoDisponible) {
        /* FALLO !: Vamos a insertar el contenido del pedido nos equivocamos con el código del producto
            * y falla la restricción que hemos creado
            */
        \$conn->query('INSERT INTO ContenidoPedidos(idPedido, idProducto, unidades) VALUES (1, -1, 1)') !== true;
        \$pedidoActualizado = \$conn->affected_rows > 0;
    }
});
} catch(\mysqli_sql_exception \$e) {
    echo "ROLLBACK ejecutado";
    \$rollback = true;
}
if (!\$rollback) {
    echo "COMMIT ejecutado";
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>

    <h1>Plantilla transacciones y plantilla SQL</h1>
    <p>Asimismo, también podemos combinar el uso de <code>MySQLTransactionTemplate</code> con
    <code>MySQLStatementTemplate</code>.</p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
namespace es\\ucm\\fdi\\aw\\sql;

// \$proveedorConexiones implementa la interfaz MySQLConnectionProvider
\$transactionTemplate = new MySQLTransactionTemplate(\$proveedorConexiones);
try {
    \$transactionTemplate->execute(function(MySQLConnectionProvider \$provider) {
        \$sqlTemplate = new MySQLStatementTemplate(\$provider);

        // Ejecutamos las operaciones

        \$filaProducto = \$sqlTemplate->queryFirst('SELECT * FROM Productos WHERE id=?', 'i', 1);

        if (\$filaProducto['unidadesDisponibles'] > 0 ) {
            // Actualizamos la tabla producto
            \$productoDisponible = \$sqlTemplate->update('UPDATE Productos SET unidadesDisponibles=(unidadesDisponibles-1) WHERE id=? AND unidadesDisponibles > 0'
                , 'i', 1) > 0;
        }

        if (\$productoDisponible) {
            /* FALLO !: Vamos a insertar el contenido del pedido nos equivocamos con el código del producto
                * y falla la restricción que hemos creado
                */
            \$pedidoActualizado = \$sqlTemplate->insert('INSERT INTO ContenidoPedidos(idPedido, idProducto, unidades) VALUES (?, ?, ?)'
                , 'iii', 1, -1, 1) > 0;
        }
    });
} catch(\mysqli_sql_exception \$e) {
    echo "ROLLBACK ejecutado";
    \$rollback = true;
}
if (!\$rollback) {
    echo "COMMIT ejecutado";
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>
</body>
</html>
