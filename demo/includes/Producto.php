<?php

namespace es\ucm\fdi\aw;

class Producto
{

  private $id;

  private $nombre;

  private $precio;

  private $unidadesDisponibles;

  public function __construct($nombre, $precio, $unidadesDisponibles, $id = NULL)
  {
    $this->nombre = $nombre;
    $this->precio = $precio;
    $this->unidadesDisponibles = $unidadesDisponibles;
    $this->id = $id;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getNombre()
  {
    return $this->nombre;
  }

  public function setNombre($nuevoNombre)
  {
    $this->nombre = $nuevoNombre;
  }

  public function getPrecio()
  {
    return $this->precio;
  }

  public function setPrecio($nuevoPrecio)
  {
    $this->precio = $nuevoPrecio;
  }

  public function getUnidadesDisponibles()
  {
    return $this->unidadesDisponibles;
  }

  public function setUnidadesDisponibles($nuevoUnidadesDisponibles)
  {
    $this->UnidadesDisponibles = $nuevoUnidadesDisponibles;
  }

  /* Métodos mágicos para que si existen métodos setPropiedad / getPropiedad se pueda hacer:
   *   $var->propiedad, que equivale a $var->getPropiedad()
   *   $var->propiedad = $valor, que equivale a $var->setPropiedad($valor)
   */
  public function __get($property)
  {
    $methodName = 'get'. ucfirst($property);
    if (method_exists($this, $methodName)) {
      return $this->$methodName();
    } else if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function __set($property, $value)
  {
    $methodName = 'set'. ucfirst($property);
    if (method_exists($this, $methodName)) {
      $this->$methodName($value);
    } else if (property_exists($this, $property)) {
      $this->$property = $value;
    }
  }

}
