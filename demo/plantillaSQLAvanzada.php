<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw;

require_once __DIR__.'/includes/config.php';

$proveedorConexiones = Aplicacion::getSingleton();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Plantilla SQL Avanzada</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js"></script>
    <!-- and it's easy to individually load additional languages -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/languages/php.min.js"></script>
    <script>
        hljs.highlightAll();
    </script>
</head>
<body>
    <h1>Plantilla para ejecutar instrucciones SQL</h1>
    <p>La clase <code>MySQLStatementTemplate</code> tiene métodos para poder invocar las operaciones habituales contra
    una base de datos MySQL implementando el método plantilla para gestionar los resultados de las operaciones.</p>
    <p>Nótese que para utilizar</p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
namespace es\\ucm\\fdi\\aw\\sql;

// \$proveedorConexiones implementa la interfaz MySQLConnectionProvider
\$sqlTemplate = new MySQLStatementTemplate(\$proveedorConexiones);
try {

// 1. Consultar una fila concreta de la BD
\$producto = \$sqlTemplate->queryFirst('SELECT * FROM Productos WHERE id=1');
echo "Producto Existente:\\n";
print_r(\$producto);

// 2. Insertar una fila en la BD y devolver la PK de la fila insertada
\$query = \sprintf("INSERT INTO Productos(nombre, precio, unidadesDisponibles) VALUES('%s',%d,%d)"
  , 'Nuevo producto'
  , round(25.10, 2)
  , 20);
\$idNuevoProducto = \$sqlTemplate->insertReturnLastId(\$query);

// 2.1 Consultar la fila insertada previamente
\$query = \sprintf('SELECT * FROM Productos WHERE id=%d'
  , \$idNuevoProducto);
\$producto = \$sqlTemplate->queryFirst(\$query);
echo "\\n\\nProducto insertado:\\n";
print_r(\$producto);

// 3. Consultar todas las filas
echo "\\n\\nTodos los productos:\\n";
\$productos = \$sqlTemplate->queryAllRows('SELECT * FROM Productos');
foreach(\$productos as \$producto) {
    print_r(\$producto);
}

// 4. Borrar la fila insertada previamente
\$query = \sprintf('DELETE FROM Productos WHERE id=%d'
  , \$idNuevoProducto);
\$resultado = \$sqlTemplate->delete(\$query);
echo "\\n\\nNúmero de filas borradas:\\n";
print_r(\$resultado);

echo "\\n\\nOK";
} catch(\mysqli_sql_exception \$e) {
    echo "KO";
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>

    <h1>Procesamiento de las filas de una consulta SELECT</h1>
    <p>Es habitual que cuando ejecutamos una instruction SELECT convirtamos las filas que nos devuelve la base de datos en objetos PHP o que las 
    procesemos de algún modo.</p>
    <p>Aunque sería posible utilizar <code>MySQLStatementTemplate::queryAllRows()</code> y procesar el array resultante, otra opción puede ser
    ir procesando los resultados según los vamos sacando del resultado de la consulta.</p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
namespace es\\ucm\\fdi\\aw\\sql;

use es\\ucm\\fdi\\aw\\Producto;

// \$proveedorConexiones implementa la interfaz MySQLConnectionProvider
\$sqlTemplate = new MySQLStatementTemplate(\$proveedorConexiones);
try {

\$producto = \$sqlTemplate->queryFirstAndProcess('SELECT * FROM Productos WHERE id=1', function(\$row){
    return new Producto(\$row['nombre'], \$row['precio'], \$row['unidadesDisponibles'], \$row['id']);
});

echo "\\n\\nProducto Existente:\\n";
print_r(\$producto);


echo "OK";
} catch(\mysqli_sql_exception \$e) {
    echo "KO";
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>

    <h1>Plantilla para ejecutar instrucciones SQL parametrizada</h1>
    <p></p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
namespace es\\ucm\\fdi\\aw\\sql;

use es\\ucm\\fdi\\aw\\Producto;

// \$proveedorConexiones implementa la interfaz MySQLConnectionProvider
\$sqlTemplate = new MySQLParameterizedStatementTemplate(\$proveedorConexiones);
try {
    
// Consulta de producto concreto
\$producto = \$sqlTemplate->queryFirst('SELECT * FROM Productos WHERE id=?', 'i', 1);
echo "\\n\\nProducto Existente:\\n";
print_r(\$producto);

// Inserción de nueva fila
\$idNuevoProducto = \$sqlTemplate->insertReturnLastId("INSERT INTO Productos(nombre, precio, unidadesDisponibles) VALUES(?,?,?)"
    , 'sdi'
    , 'Nuevo producto'
    , round(25.10, 2)
    , 20);

// Consulta de todos los productos procesando las filas
\$productos = \$sqlTemplate->queryAllAndProcess('SELECT * FROM Productos', function(\$row){
    return new Producto(\$row['nombre'], \$row['precio'], \$row['unidadesDisponibles'], \$row['id']);
});
echo "\\n\\nProductos:\\n";
print_r(\$productos);

// Consulta procesando las filas
\$producto = \$sqlTemplate->queryFirstAndProcess('SELECT * FROM Productos WHERE id=?', function(\$row){
    return new Producto(\$row['nombre'], \$row['precio'], \$row['unidadesDisponibles'], \$row['id']);
}
, 'i', 1);

echo "\\n\\nProducto Existente:\\n";
print_r(\$producto);

// Borrado del producto insertado
\$resultado = \$sqlTemplate->delete('DELETE FROM Productos WHERE id=?', 'i', \$idNuevoProducto );
echo "\\n\\nNúmero de filas borradas:\\n";
print_r(\$resultado);


echo "\\n\\nOK";
} catch(\mysqli_sql_exception \$e) {
    echo "KO";
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>
</body>
</html>
