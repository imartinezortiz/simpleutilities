<!DOCTYPE html>
<html>
<head>
    <title>Ejemplos</title>
</head>
<body>
    <h1>Ejemplos</h1>
    <p>Antes de ejecutar los ejemplos asegúrate que has cargado <a href="productosYpedidos.sql">este script SQL</a> dentro
    de la base de datos <code>test</code> de MySQL.</p>
    <p>Este proyecto tiene los siguientes ejemplos</p>
    <ol>
        <li><a href="plantillaSQL.php">Pasos necesarios para interactuar con la base de datos utilizando la interfac nativa <code>MySQLi</code>.</a></li>
        <li><a href="plantillaSQLAvanzada.php">Cómo simplificar el código utilizando el patrón método plantilla.</a> Puedes utilizarlo
        como base para implementar el patrón DAO.</li>
        <li><a href="transaccionesSimples.php">Uso y gestión de transaciones SQL de manera básica</a></li>
        <li><a href="transaccionesAvanzadas.php">Uso y gestión de transaciones SQL de manera avanzada aplicando el patrón método plantilla.</a> Puedes
        utilizarlo como base para implementar el patrón DAO.</li>
    </ol>
</body>
</html>