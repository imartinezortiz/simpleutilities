<?php
declare(strict_types=1);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Transacciones simples</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js"></script>
    <!-- and it's easy to individually load additional languages -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/languages/php.min.js"></script>
    <script>
        hljs.highlightAll();
    </script>
</head>
<body>
    <h1>Transacciones simples</h1>
    <p>Todos los ejemplso que hemos visto hasta ahora, cuando hemos ejecutado instrucciones SQL con
    <code>mysqli::query()</code> cada instrucciones se encontraba dentro de una transacción independiente.</p>
    <p>No obstante en la mayoría de aplicaciones que no son pruebas de concepto habitualmente es necesario agrupar
    más de una instrucción SQL dentro de una transacción, con el objetivo de que si alguna de las sentencias SQL
    falla ya sea porque hay un problema en el código o bien en los parámetros, las operaciones en la base de datos
    no la dejen en un estado inconsistente.</p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
// Crea la conexión a la BD
\$conn = new mysqli('localhost', 'root', '', 'test');
\$conn->begin_transaction();

\$rs = \$conn->query('SELECT * FROM Productos WHERE id=1');
\$rollback = \$rs === false;
if (!\$rollback && \$rs) {
    \$filaProducto = \$rs->fetch_assoc();
    \$rs->free();
}

if (!\$rollback && \$filaProducto['unidadesDisponibles'] > 0 ) {
    // Actualizamos la tabla producto
    \$rollback = \$conn->query('UPDATE Productos SET unidadesDisponibles=(unidadesDisponibles-1) WHERE id=1 AND unidadesDisponibles > 0') !== true;
    \$productoDisponible = \$conn->affected_rows > 0;
}

if (!\$rollback && \$productoDisponible) {
    /* FALLO !: Vamos a insertar el contenido del pedido nos equivocamos con el código del producto
        * y falla la restricción que hemos creado
        */
    \$rollback = \$conn->query('INSERT INTO ContenidoPedidos(idPedido, idProducto, unidades) VALUES (1, -1, 1)') !== true;
    \$pedidoActualizado = \$conn->affected_rows > 0;
}

if (\$rollback) {
    \$conn->rollback();
    echo "ROLLBACK ejecutado";
} else {
    \$conn->commit();
    echo "COMMIT ejecutado";
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>

    <p>El código es un poco complejo debido a que es necesario verificar si las llamadas a <code>mysqli::query()</code>
    devuelven el valor <code>false</code> y en ese caso hacer rollback. Una manera de simplificar el código es configurar
    mysqli para generar excepciones.</p>

    <h1>Transacciones gestionadas con excepciones</h1>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
// Activamos el modo de reporting basado en excepciones
\$driver = new mysqli_driver();
\$driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

// Crea la conexión a la BD
\$conn = new mysqli('localhost', 'root', '', 'test');
\$conn->begin_transaction();

try {
    // BEGIN CONTENIDO_TRANSACCION
    \$rs = \$conn->query('SELECT * FROM Productos WHERE id=1');
    if (\$rs) {
        \$filaProducto = \$rs->fetch_assoc();
        \$rs->free();
    }

    if (\$filaProducto['unidadesDisponibles'] > 0 ) {
        // Actualizamos la tabla producto
        \$conn->query('UPDATE Productos SET unidadesDisponibles=(unidadesDisponibles-1) WHERE id=1 AND unidadesDisponibles > 0') !== true;
        \$productoDisponible = \$conn->affected_rows > 0;
    }

    if (\$productoDisponible) {
        /* FALLO !: Vamos a insertar el contenido del pedido nos equivocamos con el código del producto
            * y falla la restricción que hemos creado
            */
        \$conn->query('INSERT INTO ContenidoPedidos(idPedido, idProducto, unidades) VALUES (1, -1, 1)') !== true;
        \$pedidoActualizado = \$conn->affected_rows > 0;
    }
    // END CONTENIDO_TRANSACCION
    \$conn->commit();
    echo "COMMIT ejecutado";
} catch(mysqli_sql_exception \$e) {
    // Oops, fallo, hay que hacer ROLLBACK
    if (\$conn) {
        \$conn->rollback();
        echo "ROLLBACK ejecutado";
    }
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>

    <p>Aunque usando excepciones el código se simplifica, seguimos teniendo una parte del código del ejemplo que
    tenemos que copiar y pegar en todos los lugares donde querramos utilizar transacciones. Y si no lo copiamos
    y pegamos bien, podemos tener problemas.</p>
    <p>En el ejemplo anterior lo único que es diferente para cada transacción es el cotenido que está entre los
    comentarios <code>// BEGIN CONTENIDO_TRANSACCION</code> y  <code>// END CONTENIDO_TRANSACCION</code>.</p>
    <p>Para resolver este problema podemos aplicar el patrón <em>método plantilla</em> como puede verse en
    el ejemplo <a href="transaccionesAvanzadas.php">transacciones avanzadas</a></p>
</body>
</html>
