<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw;

require_once __DIR__.'/includes/config.php';

$proveedorConexiones = Aplicacion::getSingleton();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Interacción con MySQL</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js"></script>
    <!-- and it's easy to individually load additional languages -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/languages/php.min.js"></script>
    <script>
        hljs.highlightAll();
    </script>
</head>
<body>
    <h1>Interacción con MySQL</h1>
    <p>El código PHP que es necesario para interactuar correctamente con MySQL es un código bastante homogéneo, cambiando
    habitualmente sólo aquellos aspectos que están relacionados con el <em>procesamiento de los resultados</em> de la
    sentencia SQL. En todos los casos tienes que gestionar los errores y, dependiendo de la sentencia, procesas el resultado del siguiente modo:</p>
    <ul>
        <li>Sentencia <em>SELECT</em>: Procesas la fila o filas resultantes de la consulta. Liberas los recursos asociados que no son necesarios.</li>
        <li>Sentencia <em>INSERT</em>: Verificas que el número de filas insertadas se corresponde al esperado. En algunos casos también es necesario
        obtener la clave primaria que ha asignado MySQL a una fila que hemos insertado si hemos utilizado una columna autonumérica.</li>
        <li>Sentencia <em>UPDATE</em>: Verificas que el número de filas actualizadas se corresponde al esperado.</li>
        <li>Sentencia <em>DELETE</em>: Verificas que el número de filas borradas se corresponde al esperado.</li>
    </ul>
    <p>Como podemos observar, el caso más complejo es el del procesamiento de las instrucciones SELECT. Como en el siguiente ejemplo</p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
// 1. Crea la conexión a la BD
\$conn = new mysqli('localhost', 'root', '', 'test');

// 2. Lanza la consulta
\$rs = \$conn->query('SELECT * FROM Productos');

// 3. Procesa el resultado
if (\$rs !== false) {
    // 3.1 La operación ha tenido éxito

    // 3.1.1 Procesamos los resultados
    while (\$fila = \$rs->fetch_assoc()) {
        print_r(\$fila);
        echo "\\n";
    }

    // 3.1.2 Liberamos los recursos
    \$rs->free();
} else {
    // 3.2 La operación ha fallado.
    echo "Oops, la operación ha fallado:\\n";
    echo "Error ({\$conn->errno}): {\$conn->error}\\n";
}

// 4. Cierre de la conexión (en algún momento)
\$conn->close();
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>

    <p>Una primera aproximación para simplificar el código, sería utilizar la gestión de errores basada en excepciones:</p>
    <pre><code class="php"><?php
$snippetCodigo = <<<EOS
// 0. Activamos el modo de reporting basado en excepciones
\$driver = new mysqli_driver();
\$driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

// 1. Crea la conexión a la BD
\$conn = new mysqli('localhost', 'root', '', 'test');

// 2. Lanza la consulta
try {
    \$rs = \$conn->query('SELECT * FROM Productos');

    // 3. Procesa el resultado
    // 3.1 La operación ha tenido éxito

    // 3.1.1 Procesamos los resultados
    while (\$fila = \$rs->fetch_assoc()) {
        print_r(\$fila);
        echo "\\n";
    }

    // 3.1.2 Liberamos los recursos
    \$rs->free();
} catch(mysqli_sql_exception \$e) {
    // 3.2 La operación ha fallado.
    echo "Oops, la operación ha fallado:\\n";
    echo "Error ({\$conn->errno}): {\$conn->error}\\n";
} finally {
    // 4. Cierre de la conexión (en algún momento)
    \$conn->close();
}
EOS;

echo $snippetCodigo;
?></code></pre>
    <h2>Resultado:</h2>
    <pre><samp><?php
    // XXX: OJO eval() es MUY peligroso y no se debería de usar o usar con mucho cuidado.
    eval($snippetCodigo);
    ?></samp></pre>

    <p>Aunque usando excepciones el código se simplifica, seguimos teniendo una parte del código del ejemplo que
    tenemos que copiar y pegar en todos los lugares donde querramos utilizar transacciones. Y si no lo copiamos
    y pegamos bien, podemos tener problemas.</p>

    <p>En los ejemplos anteriores podemos apreciar que el contenido que cambia al ejectuar las instrucciones SQL
    es el contenido del <em>paso 3</em>.</p>

    <p>Para simplificar nuestro código podemos aplicar el patrón <em>método plantilla</em> como puede verse en
    los ejemplos <a href="plantillaSQLAvanzada.php">uso de plantillas</a>.</p>

</body>
</html>
