<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw\sql;

/** 
 * Factoría de conexiones {@see \mysqli} simple.
 * 
 * Cada vez que se invoca `{@see SimpleMySQLConnectionProvider#getConnection()}` se crea una conexión nueva.
 */
class SimpleMySQLConnectionProvider implements MySQLConnectionProvider
{

    private $server;
    
    private $user;
    
    private $password;
    
    private $database;

    private $charset;

    public function __construct($server, $user, $password, $database, $charset= 'utf8mb4')
    {
        $this->server = $server;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
        $this->charset = $charset;
    }
    public function getConnection()
    {
        $conn = new \mysqli($this->server, $this->user, $this->password, $this->database);
        if ($conn->connect_errno) {
            throw new \Exception('Failed to connect to MySQL: '.$mysqli->connect_error, $mysqli->connect_errno);
        }
        if (!$conn->set_charset($this->charset)) {
            throw new \Exception("Failed set charset to {$this->charset}: ".$mysqli->error, $mysqli->errno);
        }
        return $conn;
    }
}
