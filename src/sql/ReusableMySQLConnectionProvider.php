<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw\sql;

/**
 * Factoría de conexiones {@see \mysqli} que crea o reutiliza una conexión existente. 
 */
class ReusableMySQLConnectionProvider  implements MySQLConnectionProvider{

    private $provider;
    private $conn;

    /**
     * 
     * @param MySQLConnectionProvider $provider Factoría utilizada para crear conexiones cuando no se haya creado una previamente.
     * @param \mysqli $conn Se inicializa esta factoría directamente con una conexión ya creada.
     */
    public function __construct(MySQLConnectionProvider $provider = null, \mysqli $conn = null)
    {
        $this->provider = $provider;
        $this->conn = $conn;
    }

    public function getConnection()
    {
        if ($this->conn === null) {
            $this->conn = $this->provider->getConnection();
        }
        return $this->conn;
    }
}
