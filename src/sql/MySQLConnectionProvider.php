<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw\sql;

/**
 * Factoría para conexiones a una base de datos MySQL o derivadas.
 */
interface MySQLConnectionProvider
{
    /**
     * Devuelve una conexión a la base de datos.
     * 
     * @return \mysqli conexión a la base de datos.
     */
    public function getConnection();
}
