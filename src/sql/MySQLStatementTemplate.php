<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw\sql;

/**
 * Plantilla para ejecutar instrucciones SQL.
 */
class MySQLStatementTemplate
{
    /**
     * Representa la invocación a {@see es\ucm\fdi\aw\sql\MySQLStatementTemplate::executeProcessResult($result)}.
     * 
     * @see MySQLStatementTemplate::executeProcessResult()
     * 
     * @var callable
     */
    protected const EXECUTE_PROCESS_RESULTS = [__CLASS__, 'executeProcessResult'];

    /**
     * Procesa los resultados de la ejecución de una instrucción SQL genérica.
     * 
     * @return bool `true` si se ha procesado exitosamente la instrucción SQL o `false` en otro caso.
     */
    protected static function executeProcessResult($result)
    {
        if (!\is_bool($result) && $result instanceof \mysqli_result ) {
            // Aunque no tiene mucho sentido se está utilizando este método para una consulta SELECT, liberamos recursos
            $result->free();
            $result = true;
        }
        return $result;
    }

    /**
     * Representa la invocación a {@see es\ucm\fdi\aw\sql\MySQLStatementTemplate::executeProcessReturnAllRows()}.
     * 
     * @see MySQLStatementTemplate::executeProcessResult()
     * 
     * @var callable
     */
    protected const EXECUTE_PROCESS_RETURN_ALL_ROWS = [__CLASS__, 'executeProcessReturnAllRows'];

    /**
     * Procesa los resultados de la ejecución de una instrucción SELECT devolviendo todas las filas resultado de la consulta.
     * 
     * @return bool `true` si se ha procesado exitosamente la instrucción SQL o `false` en otro caso.
     */
    protected static function executeProcessReturnAllRows($result, $conn)
    {
        // Se obtienen las filas de la consulta
        $results = $result->fetch_all(MYSQLI_ASSOC);
        $result->free();
        return $results;
    }

    protected const EXECUTE_PROCESS_RETURN_FIRST_ROW = [__CLASS__, 'executeReturnFirstRow'];

    protected static function executeReturnFirstRow($result, $conn)
    {
        // Se procesan los resultados
        $results =false;
        try {
            $results = $result->fetch_assoc();
        } finally {
            // Se liberan los recursos incluso en caso de error
            $result->free();
        }
        return $results;
    }

    protected const EXECUTE_PROCESS_ALL_ROWS = [__CLASS__, 'executeProcessAllRows'];

    protected static function executeProcessAllRows($result, $conn, $processRow)
    {
        // Se procesan los resultados
        $results = array();
        try {
            while ($row = $result->fetch_assoc()) {
                // Se invoca la función $processRow y se almacena el resultado
                $results[] = \call_user_func($processRow, $row);
            }
        } finally {
            // Se liberan los recursos incluso en caso de error
            $result->free();
        }
        return $results;
    }

    protected const EXECUTE_PROCESS_FIRST_ROW = [__CLASS__, 'executeProcessFirstRow'];

    protected static function executeProcessFirstRow($result, $conn, $processRow)
    {
        // Se procesan los resultados
        $results =false;
        try {
            if ($row = $result->fetch_assoc()) {
                // Se invoca la función $processRow y se almacena el resultado
                $results = \call_user_func($processRow, $row);
            }
        } finally {
            // Se liberan los recursos incluso en caso de error
            $result->free();
        }
        return $results;
    }

    protected const EXECUTE_RETURN_AFFECTED_ROWS = [__CLASS__, 'executeReturnAffectedRows'];

    protected static function executeReturnAffectedRows($result, $conn)
    {
        return $conn->affected_rows;
    }

    protected const EXECUTE_RETURN_LAST_ID = [__CLASS__, 'executeReturnLastId'];

    protected static function executeReturnLastId($result, $conn) {
        return $conn->insert_id;
    }

    /**
     * Proveedor de conexiones a la base de datos.
     * 
     * @var MySQLConnectionProvider
     */
    private $conexiones;

    /**
     * Si `true` los errores lanzan excepciones.
     * 
     * @var bool
     */
    private $errorExcepciones;

    /**
     * Construye una plantilla para ejecutar instruciones SQL contra la base de datos.
     * 
     * @param MySQLConnectionProvider $conexiones Proveedor de conexiones a la base de datos.
     * 
     * @param bool $errorExcepciones (opcional) Si `true` se lanzarán excepciones en caso de error. Nótese que si se
     * han configurado la generación de excepciones con {@see \mysqli_report()} establecer este parámetro a `false`
     * no tiene efecto.
     */
    public function __construct(MySQLConnectionProvider $conexiones, bool $errorExcepciones = true)
    {
        $this->conexiones = $conexiones;
        $this->errorExcepciones = $errorExcepciones;
    }

    /**
     * Ejecuta una sentencia SQL.
     * 
     * @param string $query Sentencia SQL.
     * 
     * @return void|bool Si esta {@see MySQLStatementTemplate} está configurada para generar excepciones en caso de
     * error no se devuelve nada. En caso contrario, devolverá `true` si la operación ha tenido éxito o `false` en
     * otro caso.
     */
    public function execute(string $query)
    {
        return $this->executeAndProcessResult($query, self::EXECUTE_PROCESS_RESULTS);
    }

    /**
     * Ejecuta una instruccion SQL y procesa el resultado de la misma.
     * 
     * El resultado de la consulta se procesa invocando `$resultProcessor` se le pasan dos parámetros:
     * 1. El resultado de ejecutar la instrucción SQL con {@see \mysqli::query()} 
     * 2. La conexión ({@see \mysqli}) utilizada para ejecutar la instrucción.
     * 
     * @param string $query Sentencia SQL.
     * @param callable $resultProcessor Función que procesará el resultado de la instrucción SQL.
     * 
     * @return mixed Si `MySQLStatementTemplate` está configurada para generar excepciones devolverá
     * el resultado de invocar `$resultProcessor`. En caso contrario, si la operación ha tenido éxito
     * devolverá el resultado de invocar `$resultProcessor` sobre el resultado de ejecutar la consulta o, en caso
     * de fallo devolverá <code>false</code>.
     */
    protected function executeAndProcessResult(string $query, callable $resultProcessor, callable $prepareQuery = null)
    {
        $result = false;
        // Se procesan los parámetros
        $sql = $query;
        if (! $sql ) {
            $result = $this->notifyError($query);            
        } else {
            // Se lanza la instrucción SQL
            $conn = $this->conexiones->getConnection();
            if ( ! \is_null($prepareQuery) && \is_callable($prepareQuery)) {
                $sql = \call_user_func($prepareQuery, $sql, $conn);
            }
            if ($sql !== false && \is_string($sql)) {
                // No hay error al procesar la query
                $result = $conn->query($sql);

                if ($result !== false) {
                    $result = \call_user_func($resultProcessor, $result, $conn);
                } else {
                    // Se procesa el error
                    $result = $this->notifyError($sql);
                }
            } else {
                $result = $this->notifyError($query);
            }
        }

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y devuelve todos los resultados.
     * 
     * El resultado se devolverá como un array de arrays, donde la segunda dimensión estarán indexados
     * por los nombres de las columnas.
     * 
     * @param string $query Sentencia SQL.
     * 
     * @return mixed Si <code>MySQLStatementTemplate</code> está configurada para generar excepciones un array de
     * arrays.En caso contrario, si la operación ha tenido éxito devolverá un array de arrays, en caso
     * de fallo devolverá <code>false</code>.
     */
    public function queryAllRows(string $query)
    {
        $result = $this->executeAndProcessResult($query, self::EXECUTE_PROCESS_RETURN_ALL_ROWS);

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y devuelve el primer resultado.
     * 
     * El resultado se devolverá como un array indexado por los nombres de las columnas.
     * 
     * @param string $query Sentencia SQL.
     * 
     * @return mixed Si <code>MySQLStatementTemplate</code> está configurada para generar excepciones un array.
     * En caso contrario, si la operación ha tenido éxito devolverá un array de arrays, en caso de fallo
     * devolverá <code>false</code>.
     */
    public function queryFirst(string $query)
    {
        $result = $this->executeAndProcessResult($query, self::EXECUTE_PROCESS_RETURN_FIRST_ROW);

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y procesa las filas resultantes con <code>$processRow</code>.
     * 
     * Las filas obtenidas como un array asociativo (e.g. los índices del array son los nombres de las columnas)
     * se pasan a la función <code>$processRow</code> como primer parámetro y el resultado que devuelva esta
     * función se añade al array de resultados.
     */
    public function queryAllAndProcess(string $query, callable $processRow)
    {
        $result = $this->executeAndProcessResult($query, function ($result, $conn) use($processRow) {
            return \call_user_func(self::EXECUTE_PROCESS_ALL_ROWS, $result, $conn, $processRow);
        });

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y devuelve el primer resultado.
     * 
     * Sólo se procesa la primera fila obtenida que se pasa a la función <code>$processRow</code>
     * como primer parámetro y el resultado que devuelva esta función se devuelve como resultado.
     */
    public function queryFirstAndProcess(string $query, callable $processRow)
    {
        $result = $this->executeAndProcessResult($query, function ($result, $conn) use($processRow) {
            return \call_user_func(self::EXECUTE_PROCESS_FIRST_ROW, $result, $conn, $processRow);
        });

        return $result;
    }

    /**
     * Ejecuta una instrucción INSERT y devuelve el número de filas insertadas
     */
    public function insert(string $query)
    {
        return $this->executeAffectedRows($query);
    }

    private function executeAffectedRows(string $query)
    {
        $result = $this->executeAndProcessResult($query, self::EXECUTE_RETURN_AFFECTED_ROWS);

        return $result;
    }

    /**
     * Ejecuta una instrucción INSERT y devuelve el identificador que se ha generado en la 
     * base de datos para la fila insertada.
     */
    public function insertReturnLastId(string $query)
    {
        $result = $this->executeAndProcessResult($query, self::EXECUTE_RETURN_LAST_ID);

        return $result;
    }

    /**
     * Ejecuta una instruccion UPDATE.
     */
    public function update(string $query)
    {
        return $this->executeAffectedRows($query);
    }

    /**
     * Ejecuta una instruccion DELETE.
     * 
     */
    public function delete(string $query)
    {
        return $this->executeAffectedRows($query);
    }

    /**
     * Gestiona un error.
     * 
     * Si {@see MySQLStatementTemplate::errorExcepciones} es `true` se lanzará una excepción si ha ocurrido un error
     * al ejecutar la operación con la BD, si es `false` se devolverá el valor `false` en caso de error. Nótese que
     * si se han configurado la generación de excepciones con {@see \mysqli_report()} establecer 
     * {@see MySQLStatementTemplate::errorExcepciones} a `false` no tiene efecto.
     * 
     * @param string $query Instrucción SQL que ha propiciado el error.
     * @param string $message (opcional) Mensaje de error que se incluirá en la excepción.
     * @param \mysqli $conn Conexión a la bd para consultar información acerca del último error.
     */
    protected function notifyError($query, $message = 'Error executing statement: ', \mysqli $conn =null) {
        $result = false;
        if ($this->errorExcepciones) {
            $databaseError = '';
            $errorCode = 500;
            if ($conn) {
                $databaseError = $conn->error;
                $errorCode =  $conn->errno;
            }
            throw new \Exception($message.$query.' error: '.$databaseError, $errorCode);
        }
        return $result;   
    }
}
