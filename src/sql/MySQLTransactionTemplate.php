<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw\sql;

/**
 * Plantilla para ejecutar instrucciones SQL dentro de una transacción.
 * 
 * @see https://dev.mysql.com/doc/refman/8.0/en/innodb-transaction-model.html MySQL 8.0 InnoDB transaction model.
 */
class MySQLTransactionTemplate
{
    /**
     * @var int Opción que representa que la transacción será de sólo lectura.
     */
    const READ_ONLY = 1;
    
    /**
     * @var int Opción que representa que la transacción será de lectura y escritura.
     */
    const READ_WRITE = 2;

    /**
     * Opción que representa que la transacción activará la lectura coherente.
     * 
     * @see https://dev.mysql.com/doc/refman/8.0/en/commit.html MySQL 8.0 START TRANSACTION, COMMIT, etc.
     * @see https://dev.mysql.com/doc/refman/8.0/en/innodb-consistent-read.html MySQL 8.0 consistent reads
     * @var int
     */
    const WITH_CONSISTENT_SNAPSHOT = 4;

    /**
     * @var int Opción que representa que la transacción tendrá el nivel de aislamiento READ UNCOMMITED.
     */
    const ISOLATION_LEVEL_READ_UNCOMMITED = 64;

    /**
     * @var int Opción que representa que la transacción tendrá el nivel de aislamiento READ COMMITED.
     */
    const ISOLATION_LEVEL_READ_COMMITED = 128;

    /**
     * @var int Opción que representa que la transacción tendrá el nivel de aislamiento REPEATIBLE READ.
     */
    const ISOLATION_LEVEL_REPEATABLE_READ = 256;

    /**
     * @var int Opción que representa que la transacción tendrá el nivel de aislamiento SERIALIZABLE.
     */
    const ISOLATION_LEVEL_SERIALIZABLE = 512;

    /**
     * Opciones por defecto de la transación si no se configuran explícitamente.
     * 
     * Por defecto tiene los valores {@see self::READ_WRITE} y {@see self::ISOLATION_LEVEL_REPEATABLE_READ}
     * @var int
     */
    const DEFAULT_TX_OPTIONS = self::READ_WRITE | self::ISOLATION_LEVEL_REPEATABLE_READ;

    /**
     * @var MySQLConnectionProvider proveedor de conexiones a la base de datos.
     */
    private $conexiones;

    /**
     * @var int Opciones de configuración de las transacciones a utilizar.
     */
    private $opcionesTransaccion;

    /**
     * Construye una plantilla para ejecutar transacciones contra la base de datos.
     * 
     * @param MySQLConnectionProvider $conexiones Proveedor de conexiones a la base de datos.
     * @param int                     $opcionesTransaccion Opciones a utilizar para las transacciones a crear.
     */
    public function __construct(MySQLConnectionProvider $conexiones, int $opcionesTransaccion = self::DEFAULT_TX_OPTIONS) {
        $this->conexiones = $conexiones;
        $this->opcionesTransaccion = $opcionesTransaccion;	
    }

    /**
     * Ejecuta la transacción.
     * 
     * Una vez inizializada la transacción se invoca <code>$operaciones</code> pasándole un {@see MySQLConnectionProvider} 
     * para obtener la conexión a la base de datos.
     * 
     * @param callable $operaciones Función callback que se invocará dentro de una transacción.
     * 
     */
    public function execute(callable $operaciones)
    {
        $conn = null;
        try {
            // Activamos el modo de reporting basado en excepciones
            $driver = new \mysqli_driver();
            $driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

            $conn = $this->conexiones->getConnection();
            if (! $conn ) {
                throw new \Exception("Connection not available");
            }
            
            // Hay que reutilizar la conexión en todas las operaciones de la transacción
            $provider = new ReusableMySQLConnectionProvider(null, $conn);

            // Configura y comienza la transacción
            self::startTransaction($conn, $this->opcionesTransaccion);

            // Ejecuta las operaciones dentro de la transacción
            $result = \call_user_func($operaciones, $provider);

            // Llegado este punto del código se puede hacer COMMIT
            $conn->commit();

            return $result;
        } catch(\mysqli_sql_exception $e) {
            // Oops, fallo, hay que hacer ROLLBACK
            if ($conn) {
                $conn->rollback();
            }
            // Se relanza la excepción para se gestione de algún modo
            throw $e;
        }
    }

    /**
     * Inicia la transacción.
     * 
     * @param \msqli $conn Conexión de base de datos a utilizar
     * @param int $txOptions Opciones para inicializar la transacción.
     */
    private static function startTransaction(\mysqli $conn, int $txOptions)
    {    
        /* 
         * Aunque en PHP >= 5.5 hay un begin_transaction() no se pueden gestionar todas las opciones disponibles
         */
        $setTransaction = 'SET TRANSACTION ISOLATION LEVEL ' . self::getIsolationLevel($txOptions);
        $conn->query($setTransaction);

        $startTransaction = 'START TRANSACTION '.self::getTransactionMode($txOptions);
        $conn->query($startTransaction);
    }

    /**
     * Devuelve el nivel de aislamiento asociado a <code>$txOptions</code>. 
     * 
     * @return string el nivel de aislamiento asociada a <code>$txOptions</code>.
     */
    private static function getIsolationLevel($txOptions)
    {
        $isolationLevel='';
        if (($txOptions & self::ISOLATION_LEVEL_SERIALIZABLE) !== 0) {
            $isolationLevel = 'SERIALIZABLE';
        } else if (($txOptions & self::ISOLATION_LEVEL_REPEATABLE_READ) !== 0) {
            $isolationLevel = 'REPEATABLE READ';
        } else if (($txOptions & self::ISOLATION_LEVEL_READ_COMMITED) !== 0) {
            $isolationLevel = 'READ COMMITTED';
        } else if (($txOptions & self::ISOLATION_LEVEL_READ_UNCOMMITED) !== 0) {
            $isolationLevel = 'READ UNCOMMITTED';
        } else {
            throw new \Exception("Isolation level not configured.");
        }
        return $isolationLevel;
    }

    /**
     * Devuelve el modo de transacción asociado a <code>$txOptions</code>. 
     * 
     * @return string el modo de transacción asociado a <code>$txOptions</code>.
     */
    private static function getTransactionMode($txOptions)
    {
        $mode='';
        if (($txOptions & self::READ_ONLY) !== 0) {
            $mode = 'READ ONLY';
        } else if (($txOptions & self::READ_WRITE) !== 0) {
            $mode = 'READ WRITE';
        } else {
            throw new \Exception("Transaction mode READ ONLY / READ REWRITE not configured.");
        }

        if (($txOptions & self::WITH_CONSISTENT_SNAPSHOT) !== 0) {
            $mode .= ', WITH CONSISTENT SNAPSHOT';
        }
        return $mode;
    }
}
