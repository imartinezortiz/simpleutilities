<?php
declare(strict_types=1);

namespace es\ucm\fdi\aw\sql;

/**
 * Plantilla para ejecutar instrucciones SQL.
 *
 */
class MySQLParameterizedStatementTemplate extends MySQLStatementTemplate
{
    /**
     * Construye una plantilla para ejecutar instruciones SQL contra la base de datos.
     *
     * @param MySQLConnectionProvider $conexiones Proveedor de conexiones a la base de datos.
     * @param bool $errorExcepciones Lanza excepciones en caso de error. Nótese que si la conexión ya está configurada
     * para lanzar excepciones {@see \mysqli_report()} establecer este parámetro a <code>false</code> no tiene efecto.
     */
    public function __construct(MySQLConnectionProvider $conexiones, bool $errorExcepciones = true)
    {
        parent::__construct($conexiones, $errorExcepciones);
    }

    /**
     * Ejecuta una sentencia SQL.
     *
     * La sentencia SQL puede estar parametrizada con '?'. En se caso, se espera que se pasen los valores
     * a sustituir a través del parámetro <code>$params</code> y el tipo de los mismos a través de
     * <code>$types</code>.
     *
     * <code>$types</code> es una cadena que que contiene tantos caracteres como parámetros a sustituir tenga
     * la consulta, por ejemplo, si su valor es <code>'iiii'</code> la consulta debe tener 4 parámetros a sustituir
     * y se esperan 4 parámetros adicionales al invocar este método.
     *
     * Los caracteres válidos para los tipos son los siguientes
     * <table>
     *   <thead>
     *     <tr>
     *       <th>Caracter</th>
     *       <th>Descripción</th>
     *     </tr>
     *   </thead>
     *   <tbody>
     *     <tr>
     *       <td>i</td>
     *       <td>El parámetro es de tipo entero</td>
     *     </tr>
     *     <tr>
     *       <td>d</td>
     *       <td>El parámetro es de tipo double</td>
     *     </tr>
     *     <tr>
     *       <td>s</td>
     *       <td>El parámetro es de tipo string</td>
     *     </tr>
     *     <tr>
     *       <td>b</td>
     *       <td>El parámetro es de tipo string que representa un BLOB en la base de datos</td>
     *     </tr>
     *   </tbody>
     * </table>
     *
     * @param string $query Sentencia SQL.
     * @param string $types (opcional) Tipos de los parámetros de la instrucción SQL.
     * @param mixed ...$params (opcional) Valores de los parámetros de la instruccion SQL.
     *
     * @return void|bool Si <code>MySQLStatementTemplate</code> está configurada para generar excepciones no devuelve
     * nada. En caso contrario, devolverá <code>true</code> si la operación ha tenido éxito o <code>false</code> en
     * otro caso.
     */
    public function execute(string $query, string $types='', ...$params)
    {
        $result = $this->executeAndProcessResultWithParameters($query, $types, $params, self::EXECUTE_PROCESS_RESULTS);

        return $result;
    }

    /**
     * Ejecuta una instruccion SQL y procesa el resultado de la misma llamando a la función `$resultProcessor`.
     *
     * Cuando se invoca `$resultProcessor` se le pasando dos parámetros, el primero es el resultado de
     * ejecutar la instrucción SQL con {@see \mysqli::query()} y el segundo es la conexión utilizada para ejecutar
     * la instrucción.
     *
     * @param string $query Sentencia SQL.
     * @param string $types (opcional) Tipos de los parámetros de la instrucción SQL.
     * @param mixed ...$params (opcional) Valores de los parámetros de la instrucción SQL.
     * @param callable $resultProcessor Función que procesará el resultado de la instrucción SQL.
     *
     * @return mixed Si <code>MySQLStatementTemplate</code> está configurada para generar excepciones devolverá
     * el resultado de invocar <code>$resultProcessor</code>. En caso contrario, si la operación ha tenido éxito
     * devolverá el resultado de invocar <code>$resultProcessor</code> sobre el resultado de ejecutar la consulta o, en caso
     * de fallo devolverá <code>false</code>.
     */
    protected function executeAndProcessResultWithParameters(string $query, string $types, array &$params, callable $resultProcessor)
    {
        $result = false;
        $result = $this->executeAndProcessResult($query, $resultProcessor, function(string $sql, \mysqli $conn) use ($types, $params) {
            $result = false;
            // Se procesan los parámetros
            $boundQuery = $this->bindParams($sql, $conn, $types, $params);
            if ($boundQuery !== false) {
                $result = $boundQuery;
            } else {
                $result = $this->notifyError($sql, 'Error binding parameters');
            }
            return $result;
        });

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y devuelve todos los resultados.
     *
     * El resultado se devolverá como un array de arrays, donde la segunda dimensión estarán indexados
     * por los nombres de las columnas.
     *
     * @param string $query Sentencia SQL.
     * @param string $types (opcional) Tipos de los parámetros de la consulta
     * @param mixed ...$params (opcional) Valores de los parámetros de la consulta
     * @param callable $resultProcessor Función que procesará el resultado de la consulta.
     *
     * @return mixed Si <code>MySQLStatementTemplate</code> está configurada para generar excepciones un array de
     * arrays.En caso contrario, si la operación ha tenido éxito devolverá un array de arrays, en caso
     * de fallo devolverá <code>false</code>.
     */
    public function queryAllRows(string $query, string $types='' , ...$params)
    {
        $result = $this->executeAndProcessResultWithParameters($query, $types, $params, self::EXECUTE_PROCESS_RETURN_ALL_ROWS);

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y devuelve el primer resultado.
     *
     * El resultado se devolverá como un array indexado por los nombres de las columnas.
     *
     * @param string $query Sentencia SQL.
     * @param string $types (opcional) Tipos de los parámetros de la consulta
     * @param mixed ...$params (opcional) Valores de los parámetros de la consulta
     * @param callable $resultProcessor Función que procesará el resultado de la consulta.
     *
     * @return mixed Si <code>MySQLStatementTemplate</code> está configurada para generar excepciones un array.
     * En caso contrario, si la operación ha tenido éxito devolverá un array de arrays, en caso de fallo
     * devolverá <code>false</code>.
     */
    public function queryFirst(string $query, string $types='' , ...$params)
    {
        $result = $this->executeAndProcessResultWithParameters($query, $types, $params, self::EXECUTE_PROCESS_RETURN_FIRST_ROW);

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y procesa las filas resultantes con <code>$processRow</code>.
     *
     * Las filas obtenidas como un array asociativo (e.g. los índices del array son los nombres de las columnas)
     * se pasan a la función <code>$processRow</code> como primer parámetro y el resultado que devuelva esta
     * función se añade al array de resultados.
     */
    public function queryAllAndProcess(string $query, callable $processRow, string $types='' , ...$params)
    {
        $result = $this->executeAndProcessResultWithParameters($query, $types, $params, function ($result, $conn) use($processRow) {
            return \call_user_func(self::EXECUTE_PROCESS_ALL_ROWS, $result, $conn, $processRow);
        });

        return $result;
    }

    /**
     * Ejecuta una sentencia SELECT y devuelve el primer resultado.
     *
     * Sólo se procesa la primera fila obtenida que se pasa a la función <code>$processRow</code>
     * como primer parámetro y el resultado que devuelva esta función se devuelve como resultado.
     */
    public function queryFirstAndProcess(string $query, callable $processRow, string $types='' , ...$params)
    {
        $result = $this->executeAndProcessResultWithParameters($query, $types, $params, function ($result, $conn) use($processRow) {
            return \call_user_func(self::EXECUTE_PROCESS_FIRST_ROW, $result, $conn, $processRow);
        });

        return $result;
    }

    /**
     * Ejecuta una instrucción INSERT y devuelve el número de filas insertadas
     */
    public function insert(string $query, string $types='' , ...$params)
    {
        return $this->executeAffectedRows($query, $types, $params);
    }

    private function executeAffectedRows(string $query, string $types='', &$params)
    {
        $result = $this->executeAndProcessResultWithParameters($query, $types, $params, self::EXECUTE_RETURN_AFFECTED_ROWS);

        return $result;
    }

    /**
     * Ejecuta una instrucción INSERT y devuelve el identificador que se ha generado en la
     * base de datos para la fila insertada.
     */
    public function insertReturnLastId(string $query, string $types='' , ...$params)
    {
        $result = $this->executeAndProcessResultWithParameters($query, $types, $params, self::EXECUTE_RETURN_LAST_ID);

        return $result;
    }

    /**
     * Ejecuta una instruccion UPDATE.
     */
    public function update(string $query, string $types='' , ...$params)
    {
        return $this->executeAffectedRows($query, $types, $params);
    }

    /**
     * Ejecuta una instruccion DELETE.
     */
    public function delete(string $query, string $types='' , ...$params)
    {
        return $this->executeAffectedRows($query, $types, $params);
    }

    private function bindParams(string $query, \mysqli $conn, string $paramsTypes, array &$params){
        $result = $query;
        $paramsCount = count($params);
        $typesLength = strlen($paramsTypes);
        if ($paramsCount != $typesLength) {
            $result = $this->notifyError($query);
            return $result;
        }
        $chunks = explode('?', $query);

        $chunksCount = count($chunks);

        if ( $chunksCount !== ($paramsCount+ 1)) {
            $result = $this->notifyError($query, 'Number of provided params is different than expected.');
            return $result;
        }

        $result ='';
        for($i=0; $i < $paramsCount; $i++) {
            $result .= $chunks[$i];
            $param=$params[$i];
            switch($paramsTypes[$i]) {
                case 'i' :
                    if (!is_int($param) && !is_null($param)) {
                        $result = $this->notifyError($query, ' Parameter: '.strval($i).' must have integer type');
                        return $result;
                    }
                    if (!is_null($param)) {
                        $result .= strval($param);
                    } else {
                        $result .= 'NULL';
                    }
                break;
                case 's' :
                    if (!is_string($param) && !is_null($param)) {
                        $result = $this->notifyError($query, ' Parameter: '.strval($i).' must have string type');
                        return $result;
                    }
                    if (!is_null($param)) {
                        $result .= "'";
                        $result .= $conn->real_escape_string($param);
                        $result .= "'";
                    } else {
                        $result .= 'NULL';
                    }
                break;
                case 'd' :
                    if (!is_double($param) && !is_null($param)) {
                        $result = $this->notifyError($query, ' Parameter: '.strval($i).' must have float type');
                    }
                    if (!is_null($param)) {
                        $result .= strval($param);
                    } else {
                        $result .= 'NULL';
                    }
                break;
                case 'b' :
                    if (!is_string($param) && !is_null($param)) {
                        $result = $this->notifyError($query, ' Parameter: '.strval($i).' must be a string representation of the blob value');
                        return $result;
                    }
                    if (!is_null($param)) {
                        $result .= "'";
                        $result .= $conn->real_escape_string($param);
                        $result .= "'";
                    } else {
                        $result .= 'NULL';
                    }
                break;
                default :
                    $result = $this->notifyError($query, ' Parameter: '.strval($i).' has an unexpected type: '.$paramsTypes[$i]);
                    return $result;
                break;
            }
        }
        $result .= $chunks[$paramsCount];

        return $result;
    }
}