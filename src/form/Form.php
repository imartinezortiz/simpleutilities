<?php

namespace es\ucm\fdi\aw\form;


use es\ucm\fdi\aw\http\HttpUtils;

/**
 * Clase base para la gestión de formularios.
 *
 * Además de la gestión básica de los formularios, también se utilizan tokens para controlar ataques CSRF y envíos
 * múltiples (e.g. doble click) del formulario.
 */
abstract class Form
{

    /**
     * @var string Sufijo para el nombre del parámetro de la sesión del usuario donde se almacena el token CSRF.
     */
    const CSRF_SESSION_PARAM = 'csrf';
    
    /**
     * @var string Nombre del parámetro que se se envía en el formulario que contiene el token CSRF.
     */
    const CSRF_REQUEST_PARAM = 'CSRFToken';
    
    /**
     * @var string Nombre del parámetro para acceder de la cabecera <code>X-XSRF-TOKEN</code> que se debe enviar
     * en una petición AJAX y cuyo valor debe ser el token CSRF almacenado en el campo del formulario cuyo nombre está
     * definido en la constante <code>self::CSRF_REQUEST_PARAM</code>.
     */
    const CSRF_AJAX_HEADER = 'HTTP_X_XSRF_TOKEN'; 

    /**
     * @var string Cadena utilizada como valor del atributo "id" de la etiqueta &lt;form&gt; asociada al formulario y 
     * como parámetro a comprobar para verificar que el usuario ha enviado el formulario.
     */
    private $formId;

    /**
     * @var boolean Almacena si el envío del formulario se va a realizar mediante una petición AJAX.
     */
    private $ajax;

    /**
     * @var string URL asociada al atributo "action" de la etiqueta &lt;form&gt; del fomrulario y que procesará el 
     * envío del formulario.
     */
    private $action;

    /**
     * @var string Valor del atributo "class" de la etiqueta &lt;form&gt; asociada al formulario. Si este parámetro
     * incluye la cadena "nocsrf" no se generá el token CSRF para este formulario.
     */
    private $classAtt;

    /**
     * @var string Valor del parámetro enctype del formulario.
     */
    private $enctype;

    /**
     * Crea un nuevo formulario.
     *
     * Posibles opciones:
     * <table>
     *   <thead>
     *     <tr>
     *       <th>Opción</th>
     *       <th>Valor por defecto</th>
     *       <th>Descripción</th>
     *     </tr>
     *   </thead>
     *   <tbody>
     *     <tr>
     *       <td>action</td>
     *       <td><code>$_SERVER['PHP_SELF']</code></td>       
     *       <td>URL asociada al atributo "action" de la etiqueta &lt;form&gt; del fomrulario y que procesará
                 el envío del formulario.</td>
     *     </tr>
     *     <tr>
     *       <td>class</td>
     *       <td><code>null</code></td>
     *       <td>Valor del atributo "class" de la etiqueta &lt;form&gt; asociadaal formulario. Si este
                 parámetro incluye la cadena "nocsrf" no se generá el token CSRF para este formulario.</td>
     *     </tr>
     *     <tr>
     *       <td>action</td>
     *       <td><code>null</code></td>
     *       <td>Valor del parámetro enctype del formulario</td>
     *     </tr>
     *     <tr>
     *       <td>ajax</td>
     *       <td><code>false</code></td>
     *       <td>Si <code>$opciones['ajax']===true</code> el procesamiento del formulario o la gestión de errores
     *           no gestiona la petición de la manera habitual sino devuelve el resultado de <code>Form#procesaFormulario(array)</code>
     *           en caso de éxito o <code>Form#generaErroresAjax(array)</code> en caso de error.</td>
     *     </tr>
     *   </tbody>
     * </table>

     * @param string $formId    Cadena utilizada como valor del atributo "id" de la etiqueta &lt;form&gt; asociada al
     *                          formulario y como parámetro a comprobar para verificar que el usuario ha enviado el formulario.
     *
     * @param array $opciones (ver más arriba).
     */
    public function __construct($formId, $opciones = array() )
    {
        $this->formId = $formId;

        $opcionesPorDefecto = array( 'ajax' => false, 'action' => null, 'class' => null, 'enctype' => null );
        $opciones = array_merge($opcionesPorDefecto, $opciones);

        $this->ajax     = (boolean)$opciones['ajax'];
        $this->action   = $opciones['action'];
        $this->classAtt = $opciones['class'];
        $this->enctype  = $opciones['enctype'];
        
        if ( !$this->action ) {
            $this->action = htmlspecialchars ($_SERVER['REQUEST_URI']);
        }
    }
  
    /**
     * Se encarga de orquestar todo el proceso de gestión de un formulario.
     */
    public function gestiona()
    {
    
        if ( ! $this->formularioEnviado($_POST) ) {
            echo $this->generaFormulario();
        } else {
            // Valida el token CSRF si es necesario (hay un token en la sesión asociada al formulario)
            $tokenRecibido = isset($_POST[self::CSRF_REQUEST_PARAM]) ? $_POST[self::CSRF_REQUEST_PARAM] : false;
            if ( ! $tokenRecibido && $this->ajax ) {
                $tokenRecibido = isset($_SERVER[self::CSRF_AJAX_HEADER]) ? $_SERVER[self::CSRF_AJAX_HEADER] : false;
            }                

            if ( ($errores = $this->csrfguardValidateToken($this->formId, $tokenRecibido)) !== true ) { 
                if ( ! $this->ajax ) {
                    echo $this->generaFormulario($errores, $_POST);
                } else {
                    echo $this->generaErroresAjax($errores);
                }
            } else  {
                $result = $this->procesaFormulario($_POST);
                if ( is_array($result) ) {
                    // Error al procesar el formulario, volvemos a mostrarlo
                    if ( ! $this->ajax ) {
                        echo $this->generaFormulario($result, $_POST);
                    } else {
                        echo $this->generaErroresAjax($result);
                    }
                } else {
                    if ( ! $this->ajax ) {
                        HttpUtils::redirige(.$result);
                    } else {
                        echo $result;
                    }
                }
            }
        }  
    }

    /**
     * Genera el HTML necesario para presentar los campos del formulario.
     *
     * @param string[] $datos Datos iniciales para los campos del formulario (normalmente <code>$_POST</code>).
     * @param string[] $errores (opcional) Array con los mensajes de error de validación y/o procesamiento del formulario.
     * 
     * @return string HTML asociado a los campos del formulario.
     */
    protected function generaCamposFormulario($datos, $errores=array())
    {
        return '';
    }

    /**
     * Procesa los datos del formulario.
     *
     * @param string[] $datos Datos enviado por el usuario (normalmente <code>$_POST</code>).
     *
     * @return string|string[] Devuelve el resultado del procesamiento del formulario, normalmente una URL a la que
     * se desea que se redirija al usuario, o una lista con los errores que ha habido durante el procesamiento del formulario.
     */
    protected function procesaFormulario($datos)
    {
        return array();
    }

    /**
     * Genera la salida que espera la llamada AJAX que ha enviado el formulario.
     *
     * @param string[] $errores Array con los mensajes de error de validación y/o procesamiento del formulario.
     *
     * @return string Cadena (con la representación oportuna como JSON) de los errores de validación.
     */
    protected function generaErroresAjax($errores)
    {
        return '';
    }
  
    /**
     * Función que verifica si el usuario ha enviado el formulario.
     * Comprueba si existe el parámetro <code>$formId</code> en <code>$params</code>.
     *
     * @param string[] $params Array que contiene los datos recibidos en el envío formulario.
     *
     * @return boolean Devuelve <code>true</code> si <code>$formId</code> existe como clave en <code>$params</code>
     */
    private function formularioEnviado(&$params)
    {
        return isset($params['action']) && $params['action'] == $this->formId;
    } 

    /**
     * Función que genera el HTML necesario para el formulario.
     *
     * @param string[] $errores (opcional) Array con los mensajes de error de validación y/o procesamiento del formulario.
     *
     * @param string[] $datos (opcional) Array con los valores por defecto de los campos del formulario.
     *
     * @return string HTML asociado al formulario.
     */
    private function generaFormulario($errores = array(), &$datos = array())
    {

        $html= $this->generaListaErrores($errores);

        $html .= '<form method="POST" action="'.$this->action.'" id="'.$this->formId.'"';
        if ( $this->classAtt ) {
            $html .= ' class="'.$this->classAtt.'"';
        }
        if ( $this->enctype ) {
            $html .= ' enctype="'.$this->enctype.'"';
        }
        $html .=' >';

        // Se genera el token CSRF si el usuario no solicita explícitamente lo contrario.
        if ( ! $this->classAtt || strpos($this->classAtt, 'nocsrf') === false ) {
            $tokenValue = $this->csrfguardGenerateToken($this->formId);
            $html .= '<input type="hidden" name="'.self::CSRF_REQUEST_PARAM.'" value="'.$tokenValue.'" />';
        }

        $html .= '<input type="hidden" name="action" value="'.$this->formId.'" />';

        $html .= $this->generaCamposFormulario($datos, $errores);
        $html .= '</form>';
        return $html;
    }

    /**
     * Genera la lista de mensajes de error a incluir en el formulario.
     *
     * @param string[] $errores (opcional) Array con los mensajes de error de validación y/o procesamiento del formulario.
     *
     * @return string El HTML asociado a los mensajes de error.
     */
    private function generaListaErrores($errores)
    {
        $html='';
        $numErrores = count($errores);
        if (  $numErrores == 1 ) {
            $html .= "<ul><li>".$errores[0]."</li></ul>";
        } else if ( $numErrores > 1 ) {
            $html .= "<ul><li>";
            $html .= implode("</li><li>", $errores);
            $html .= "</li></ul>";
        }
        return $html;
    }

    /**
     * Genera una cadena aleatoria de 128 caracteres que se utilizará como token CSRF.
     * Este valor se almacenará en la <code>$_SESSION</code> del usuario para poder posteriormente comprobarlo.
     *
     * @param string $formId Id del formulario para que
     * @see https://www.owasp.org/index.php/PHP_CSRF_Guard Información original en la que está basada el código.
     *
     * @return string Devuelve el token CSRF a incluir en el formulario.
     *
     * @throws RuntimeException Si no es posible guardar el token en la sesión del usuario.
     */
    private function csrfguardGenerateToken($formId)
    {
        if ( ! isset($_SESSION) ) {
            throw new \RuntimeException('La sesión del usuario no está definida.');
        }
    
        if ( function_exists('hash_algos') && in_array('sha512', hash_algos()) ) {
            $token = hash('sha512', mt_rand(0, mt_getrandmax()));
        }	else {
            $token=' ';
            for ($i=0;$i<128;++$i) {
                $r=mt_rand(0,35);
                if ($r<26){
                    $c=chr(ord('a')+$r);
                }	else{ 
                    $c=chr(ord('0')+$r-26);
                } 
                $token.=$c;
            }
        }

        $_SESSION[$formId.'_'.self::CSRF_SESSION_PARAM]=$token;

        return $token;
    }

    /**
     * Valida el token CSRF recibido.
     *
     * @param string $formId Id del formulario a comprobar.
     * @param string $tokenRecibido Token CSRF recibido en la petición.
     *
     * @return boolean|string[] <code>true</code> si la validación es satisfactoria o un array que contiene una lista de
     * mensajes de error.
     *
     * @throws RuntimeException Si no es posible acceder a la sesión del usuario.
     */
    private function csrfguardValidateToken($formId, $tokenRecibido)
    {
        if ( ! isset($_SESSION) ) {
            throw new \RuntimeException('La sesión del usuario no está definida.');
        }
    
        $result = true;
    
        if ( isset($_SESSION[$formId.'_'.self::CSRF_SESSION_PARAM]) ) {
            if ( $_SESSION[$formId.'_'.self::CSRF_SESSION_PARAM] !== $tokenRecibido ) {
                $result = array();
                $result[] = 'Has enviado el formulario dos veces';
            }
            $_SESSION[$formId.'_'.self::CSRF_SESSION_PARAM] = ' ';
            unset($_SESSION[$formId.'_'.self::CSRF_SESSION_PARAM]);
        } else {
            $result = array();
            $result[] = 'Formulario no válido';
        }
        return $result;
    }
}
